# README #

Write test cases for the following Features:
1. Everything:
When all news of ‘Covid’ is requested from a source of “BBC News”, with a given page size, validate
the following for each article:<br><br>

1.Following fields should not be empty – source name, title, description, and URL.<br>
2.Title should contain the word ‘Covid’.<br>
3.Source id should match the same as requested.<br>
4.URLs are from the actual news domain<br>
5.Number of articles returned in the response should match the page size requested. 


